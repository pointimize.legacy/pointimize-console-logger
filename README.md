# pointimize-console-logger

A very simple console log with log levels and colored label.

[![npm version](https://badge.fury.io/js/pointimize-console-logger.svg)](https://badge.fury.io/js/pointimize-console-logger)

## Installation
```
npm install pointimize-console-logger --save
```

## Examples
```javascript
var log = require('pointimize-console-logger')( { level: 'trace' });

log.trace('trace log');
log.debug('debug log');
log.info('info log');
log.warn('warn log');
log.error('error log');
log.success('success log');
```
![](example/example.png)

## Options
`level`: log level, one of `trace`, `debug`, `info`, `warn`, `error`, and `success`.

### level
A string to specify the log level. Defaults to `info`.

## License
MIT
